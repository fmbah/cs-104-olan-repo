provider "aws" {
	region = "eu-west-1"
	access_key = "AKIAT43NF4GSJIIXHC5M"
	secret_key = "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"
}

resource "aws_instance" "cs-104-lession6-Olan" {
  instance_type = "t2.micro"      
  ami = "ami-04f5641b0d178a27a"   
  key_name = "dev-mindz"   

tags = {
  Name = "cs-104-lession6-Olan"
}

}
resource "aws_security_group" "http-ssh-allowed" {
  name = "dev-mindz-sec-group"
  description = "Security Group for the SSH"      
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "SSH"
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTP"
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTP"
    from_port = 3000
    protocol = "tcp"
    to_port = 3000
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_vpc" "dev-mindz" {
    cidr_block = "10.0.0.0/16"
    enable_dns_support = "true" #gives you an internal domain name
    enable_dns_hostnames = "true" #gives you an internal host name
    enable_classiclink = "false"
    instance_tenancy = "default"    
    
    tags = {
        Name = "dev-mindz"
    }
}

resource "aws_subnet" "dev-mindz-subnet-public-1" {
    vpc_id = aws_vpc.dev-mindz.id
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = "true" //it makes this a public subnet
    availability_zone = "eu-west-1a"

    tags = {
        Name = "dev-mindz-subnet-public-1"
    }
}

resource "aws_subnet" "dev-mindz-subnet-public-2" {
    vpc_id = aws_vpc.dev-mindz.id
    cidr_block = "10.0.2.0/24"
    map_public_ip_on_launch = "true" //it makes this a public subnet
    availability_zone = "eu-west-1a"

    tags = {
        Name = "dev-mindz-subnet-public-2"
    }
}

resource "aws_subnet" "dev-mindz-subnet-private-1" {
    vpc_id = aws_vpc.dev-mindz.id
    cidr_block = "10.0.3.0/24"
    map_public_ip_on_launch = "false" //it makes this a public subnet
    availability_zone = "eu-west-1b"

    tags = {
        Name = "dev-mindz-subnet-private-1"
    }
}

resource "aws_subnet" "dev-mindz-subnet-private-2" {
    vpc_id = aws_vpc.dev-mindz.id
    cidr_block = "10.0.4.0/24"
    map_public_ip_on_launch = "false" //it makes this a public subnet
    availability_zone = "eu-west-1b"

    tags = {
        Name = "dev-mindz-subnet-private-2"
    }
 }

resource "aws_internet_gateway" "default" {
   vpc_id = aws_vpc.dev-mindz.id
}

USING JENKINSTO QUERY AWS ENVIRONMENT

N.B Ensure you can locally connect to your AWS Environment by installing AWS CLI and configure the AWS connectivity.

Step 1:

Create a Jenkins file within your current Project Directory name it Jenkinsfile. 

Copy the Jenkins Script in this Repo and make a file off it in your local

This script puts into consideration different constructs like: Parameter, Conditions, Boolean, Agents, and Parallel cnstr.

Step 2:

Run simple groovy script command to confirm connectivity to the AWS Environment like: aws s3 ls

Step 3:

Save your Jenkinsfile and Go to Jenkins Dashboard to build a new Bitbucket pipeline. 

Select github project and check “This project is parameterized” option. 

Set the required Jenkinsfile file path, Apply and Save your pipeline.

Step 4:

Finally, Run the build for your project and monitor the script output either on Jenkins or Blue Ocean to confirm no error is generated. 

If there is any error, check your syntax as this could be the likely cause. Once the build is successful. 

You have successfully built your Pipeline!



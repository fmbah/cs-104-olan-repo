#!/usr/bin/env bash
echo "*******This shell script uses a AWS Bastion Host to query info*****" 
echo "******* on the Dev-Minds AWS Environment*****" 
time
echo "-----------------------------------------------------------------------"
echo "                           List of VPCs                                "
echo "-----------------------------------------------------------------------"
aws ec2 describe-vpcs
echo "-----------------------------------------------------------------------"
echo "                           Organization                               "
echo "----------------------------------------------------------------------"
aws organizations list-accounts
echo "----------------------------------------------------------------------"
echo "                          Subnets per VPC                             "
echo "----------------------------------------------------------------------"
aws ec2 describe-subnets
echo "----------------------------------------------------------------------"
echo "                             IAM Users                                "
echo "----------------------------------------------------------------------"
aws iam list-users
echo "----------------------------------------------------------------------"
echo "                             S3 Buckets                               "
echo "----------------------------------------------------------------------"
aws s3 ls
echo "----------------------------------------------------------------------"
echo "                               EC2                                    "
echo "-----------------------------------------------------------------------"
aws ec2 describe-instances
echo "-----------------------------------------------------------------------"
echo "                        Route53 hosted zones                           "
echo "-----------------------------------------------------------------------"
aws route53 list-hosted-zones

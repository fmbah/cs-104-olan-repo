resource "aws_subnet" "private_1" {
  vpc_id     = aws_vpc.vpc_prod.id
  map_public_ip_on_launch = false
  cidr_block = "172.31.27.0/24"

  tags = {
    Name = "private_1-prod"
  }
}
resource "aws_subnet" "private_2" {
  vpc_id     = aws_vpc.vpc_prod.id
  map_public_ip_on_launch = false
  cidr_block = "172.31.28.0/24"

  tags = {
    Name = "private_2-prod"
  }
}
resource "aws_subnet" "private_3" {
  vpc_id     = aws_vpc.vpc_prod.id
  map_public_ip_on_launch = false
  cidr_block = "172.31.29.0/24"

  tags = {
    Name = "private_3-prod"
  }
}

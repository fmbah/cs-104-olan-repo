provider "aws" {
  version = ">= 0.12.31"
  profile = "${terraform.workspace}"
}

module "vpc" {
  source = "./vpc-module"
}

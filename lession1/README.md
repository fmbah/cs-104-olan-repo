**This sample project provisions a cron job report that runs some system utility check on host machine by 11am everyday **

**The result is then outputed into a txt file for reporting **

** Note that the Report overwrites the previous day to ensure space management on the host machine **

** Feel free to contribute to this project **


resource "aws_s3_bucket" "state-storage" {
  bucket_prefix = var.bucket_prefix
  acl           = var.acl_value
  region        = var.aws_region
  tags          = var.tags

  versioning {
    enabled = true
  }
}

terraform {
  backend "s3" {
    region         = var.aws_region
    encrypt        = true
    bucket         = var.bucket
    key            = "./terraform.tfstate"
  }
}


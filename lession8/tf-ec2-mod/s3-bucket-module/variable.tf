variable "bucket_prefix" {
  type        = string
  description = "middle-ware"
  default     = "frontendteam"
}

variable "aws_region" {
  default = "eu-west-1"
}

variable "aws_profile" {
  default = ""
}

variable "acl_value" {
  type    = string
  default = "private"
}

variable "versioning" {
  type        = bool
  description = "true or false"
  default     = "true"
}

variable "tags" {
  type        = map(any)
  description = "tagging"

  default = {
    environment = "preprod"
    terraform   = "true"
  }
}

provider "aws" {
  version = "~>2.0"
  region  = "eu-west-1"
  profile = "${terraform.workspace}" 
}

module "s3_bucket" {
  source = "./s3-bucket-module"
   

}

variable "aws_region" {
    default = "eu-west-1"
}

variable "aws_profile" {
	default = ""
}

variable "iam-username" {
    default = "frontend"
 }
